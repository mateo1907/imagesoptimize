<?php
if (!defined('_PS_VERSION_')) {
  exit;
}
require 'vendor/autoload.php';
define('SMUSHIT_URL', 'http://api.resmush.it/ws.php?img=');
define('SMUSHIT_LOGS',dirname(__FILE__).'/logs/');

class ImagesOptimize extends Module
{

  protected $imageTypes = [];
  protected $quality;

  public function __construct()
  {
    $this->name = 'imagesoptimize';
    $this->tab = 'administration';
    $this->version = '1.0.1';
    $this->author = 'Mateusz Prasał';
    $this->need_instance = 1;
    $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    $this->bootstrap = true;

    parent::__construct();

    $this->displayName = $this->l('Images Optimize');
    $this->description = $this->l('Optimize Your product images with Smush It API');

    $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

    if (!Configuration::get($this->name)) {
      $this->warning = $this->l('No name provided');
    }

    foreach (ImageType::getImagesTypes('products') as $type) {
      $this->imageTypes[] = $type;
    }

    $this->quality = Configuration::get($this->name . '_quality');
  }

  public function install()
  {
    if (!parent::install() || !$this->registerHook('actionWatermark')  ) {
      return false;
    }
    Configuration::updateValue($this->name,$this->name);
    return true;
  }

  public function uninstall()
  {
      return (parent::uninstall() && Configuration::deleteByName($this->name));
  }

  public function getContent()
  {

    return  $this->_postProcess() . $this->_displayForm();
  }

  public function _postProcess()
  {
    $output = '';
    $errors = [];
    if (Tools::isSubmit('submit'.$this->name)) {

      if ((int)Tools::getValue($this->name . '_quality') == 0) {

        $errors[] = $this->displayError($this->l('Pole musi być liczbą!'));
      }

      if ((int)Tools::getValue($this->name . '_quality') > 100) {
        $errors[] = $this->displayError($this->l('Wartość nie może być większa od 100'));
      }


      if (empty($errors)) {
        Configuration::updateValue($this->name . '_quality', Tools::getValue($this->name . '_quality'));
        $output .= $this->displayConfirmation($this->l('Zaktualizowano!'));

      } else {
        foreach ($errors as $error) {
          $output .= $error;
        }
      }
      return $output;


    }

  }

  public function _displayForm()
  {
    $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
      $helper = new HelperForm();

      // Module, token and currentIndex
      $helper->module = $this;
      $helper->name_controller = $this->name;
      $helper->token = Tools::getAdminTokenLite('AdminModules');
      $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

      // Language
      $helper->default_form_language = $default_lang;
      $helper->allow_employee_form_lang = $default_lang;

      // Title and toolbar
      $helper->title = $this->displayName;
      $helper->show_toolbar = true;        // false -> remove toolbar
      $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
      $helper->submit_action = 'submit'.$this->name;
      $helper->toolbar_btn = array(
          'save' =>
          array(
              'desc' => $this->l('Save'),
              'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
              '&token='.Tools::getAdminTokenLite('AdminModules'),
          ),
          'back' => array(
              'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
              'desc' => $this->l('Back to list')
          )
      );

      // Load current value
      $helper->fields_value[$this->name . '_quality'] = Configuration::get($this->name . '_quality');

      return $helper->generateForm($this->_renderForm());
  }

  public function _renderForm()
  {
    $fields_form[0]['form'] = array(
        'legend' => array(
            'title' => $this->l('Ustawienia'),
        ),
        'input' => array(
            array(
                'type' => 'text',
                'label' => $this->l('Jakość'),
                'name' => $this->name . '_quality',
                'required' => true
            )
        ),
        'submit' => array(
            'title' => $this->l('Zapisz'),
            'class' => 'btn btn-default pull-right'
        )
      );

      return $fields_form;
  }

  public function hookActionWatermark($params)
  {

    global $cookie;

    $images_list = array();
    $image = new Image($params['id_image']);

    $file = _PS_PROD_IMG_DIR_.$image->getExistingImgPath() . '.jpg' ;
    $url = _PS_BASE_URL_._THEME_PROD_DIR_ . $image->getExistingImgPath() . '.jpg';

    $this->optimizeImage($url, $file);

    // goin through all image thumbnails
    foreach ($this->imageTypes as $type) {
      $file = _PS_PROD_IMG_DIR_.$image->getExistingImgPath() . '-' . Tools::stripslashes($type['name']) . '.jpg';
      $url = _PS_BASE_URL_._THEME_PROD_DIR_ . $image->getExistingImgPath() . '-'. Tools::stripslashes($type['name']) .'.jpg';
      $this->optimizeImage($url,$file);
    }



  }

  public function optimizeImage($oldImageUrl,$oldImageFile)
  {
    $date = new DateTime;

    $curl = new \Curl\Curl();
    $img = $curl->get(SMUSHIT_URL . $oldImageUrl . '&qlty=' . $this->quality );

    $data = $date->format('Y-m-d H:i:s') . ' => ' . print_r($img,true);
    file_put_contents(SMUSHIT_LOGS . 'log_'.$date->format('Y-m-d').'.txt', $data . PHP_EOL ,FILE_APPEND );

    Tools::copy($img->dest, $oldImageFile );

    return true;
  }
}
